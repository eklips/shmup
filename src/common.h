#ifndef COMMON_H
#define COMMON_H
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdarg.h>

#define WINDOW_HEIGHT (680)
#define WINDOW_WIDTH (840)

typedef enum {false, true} bool;

typedef struct gw {
    SDL_Window* win;
    SDL_Renderer* rend;
} GameWindow;

#ifdef DEBUG
#define debug_printf(...) printf(__VA_ARGS__)
#else
#define debug_printf(...) NULL
#endif

#endif /* COMMON_H */