#include <stdio.h>
#ifdef __linux__
#include <unistd.h>
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "engine.h"

GameWindow* init_sdl(char* window_title, char* window_icon)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("Error initializing SDL: %s\n", SDL_GetError());
        return NULL;
    }
    debug_printf("SDL successfully initialized\n");

    GameWindow* screen = malloc(sizeof(SDL_Window*)+sizeof(SDL_Renderer*));
    screen->win = SDL_CreateWindow(window_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                       WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE);
    if (!screen->win) {
        printf("Failed to create window: %s\n", SDL_GetError());
        free(screen);
        return NULL;
    }

    if (window_icon != NULL) {
        if (access(window_icon, R_OK) == 0) {
              SDL_Surface* icon_img = IMG_Load(window_icon);
              SDL_SetWindowIcon(screen->win, icon_img);
              SDL_FreeSurface(icon_img);
        }
        else printf("init_sdl: Icon %s not found\n", window_icon);
    }
    
    screen->rend = SDL_CreateRenderer(screen->win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!screen->rend) {
        printf("Failed to create renderer: %s\n", SDL_GetError());
        free(screen);
        return NULL;
    }

    return screen;
}
