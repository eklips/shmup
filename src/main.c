#include <stdio.h>
#include "engine.h"

int main(int argc, char** argv) 
{
    printf("Starting...\n");
    GameWindow* mainwin = init_sdl("my shmup", NULL);
    if (!mainwin) {
        printf("Fatal error\nExiting...\n");
        return 1;
    }

    // main game loop would go here
    
    free(mainwin);
    return 0;
}