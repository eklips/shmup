CC := gcc

ODIR_RELEASE = obj_release
ODIR_DEBUG = obj_debug
SDIR = src

DEFS_RELEASE = -DNDEBUG
DEFS_DEBUG = 

LIBS := -lSDL2 -lSDL2_image

CFLAGS_RELEASE := -Os -ffast-math
CFLAGS_DEBUG := -g3

LFLAGS_RELEASE := -s
LFLAGS_DEBUG := 

SRCS := $(wildcard $(SDIR)/*.c)

OBJS_RELEASE := $(subst $(SDIR),$(ODIR_RELEASE),$(SRCS:.c=.o))
OBJS_DEBUG := $(subst $(SDIR),$(ODIR_DEBUG),$(SRCS:.c=.o))

RELEASE_EXEC := shmup_release
DEBUG_EXEC := shmup_debug

$(ODIR_RELEASE)/%.o: $(SDIR)/%.c
	$(CC) $(DEFS_RELEASE) -o $@ -c $< $(CFLAGS_RELEASE)

$(ODIR_DEBUG)/%.o: $(SDIR)/%.c
	$(CC) $(DEFS_DEBUG) -o $@ -c $< $(CFLAGS_DEBUG)

$(RELEASE_EXEC): create_release_dir $(OBJS_RELEASE) Makefile
	$(CC) -o $@ $(OBJS_RELEASE) $(LFLAGS_RELEASE) $(LIBS)

$(DEBUG_EXEC): create_debug_dir $(OBJS_DEBUG) Makefile
	$(CC) -o $@ $(OBJS_DEBUG) $(LFLAGS_DEBUG) $(LIBS)

release: $(RELEASE_EXEC)
r: release
debug: $(DEBUG_EXEC)
d: debug

all: release debug

create_release_dir:
	mkdir -p $(ODIR_RELEASE)

create_debug_dir:
	mkdir -p $(ODIR_DEBUG)

create_dirs: create_release_dir create_debug_dir

clean: clean_executables clean_objs

clean_executables:
	rm -f $(RELEASE_EXEC) $(DEBUG_EXEC)

clean_release_exec:
	rm -f $(RELEASE_EXEC)

clean_debug_exec:
	rm -f $(DEBUG_EXEC)

clean_objs: clean_objs_release clean_objs_debug

clean_objs_release:
	rm -rf $(ODIR_RELEASE)

clean_objs_debug:
	rm -rf $(ODIR_DEBUG)
